const webpack = require('webpack');
const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const nodeModulesPath = path.resolve(__dirname, 'node_modules');

const isDev = process.env.NODE_ENV === 'development';

const commonConfig = {
    entry: './src/index.jsx',
    mode: 'development',
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: '[name].[hash].js',
    },
    optimization:{
        splitChunks: {
            chunks: 'all'
        }
    },
    resolve: {
        extensions: [ '.jsx', '.js' ]
    },
    module: {
        rules: [
            {
                test: /\.(scss|sass)$/,
                exclude: /node_modules/,
                loaders: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader',]
            },
            {
                test: /\.css$/,
                loaders: ['style-loader', MiniCssExtractPlugin.loader, 'css-loader']
            },
            {
                test: /\.jsx?$/,
                use: ['babel-loader'],
                exclude: nodeModulesPath,
            },
            {
                test: /\.(jpg|jpeg|png|eot|ttf|otf|woff|woff2|svg)$/,
                loader: `file-loader`,
                options: {
                    name: '[md5:hash:hex:30].[ext]',
                },
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html',
            inject: 'body',
            filename: 'index.html',
            hash: true,
        }),
        new MiniCssExtractPlugin({
            filename: '[name].[hash].css',
            chunkFilename: '[id].[hash].css',
        }),
        new CleanWebpackPlugin()
    ],
    devServer: {
        contentBase: false,
        index: 'index.html',
        historyApiFallback: true,
        open: false,
        host: '0.0.0.0',
    },
    devtool: isDev ? 'source-map' : '',
};

module.exports = () => {
    return [commonConfig];
};
