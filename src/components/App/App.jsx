import React from 'react';
import './index.scss';
import Interface from '../Interface';

export default class App extends React.Component {
    render() {
        return (
            <div className="App">
                <Interface />
            </div>
        );
    }
}
