import React from 'react';
import MarketIcon from '../../../assets/icons/market.svg';

import './TopContent.scss';

const TopContent = () => (
    <div className="TopContainer">
        <ContainerItem />
        <ContainerItem />
        <ContainerItem />
        <ContainerItem />
        <ContainerItem />
        <ContainerItem />
        <ContainerItem />
        <ContainerItem />
    </div>
);

const ContainerItem = () => (
    <div className="TopContainer_item">
        <img className="TopContainer_item__icon" src={MarketIcon} />
    </div>
);

export default TopContent;
