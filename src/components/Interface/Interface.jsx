import React from 'react';
import './Interface.scss';
import TopContent from './TopContent';

const Interface = ({...props}) => {
    return (
        <div className="Interface">
            <TopContent />
        </div>
    );
};

export default Interface;
